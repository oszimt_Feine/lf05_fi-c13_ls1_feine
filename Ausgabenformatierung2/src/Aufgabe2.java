
public class Aufgabe2 {

	public static void main(String[] args) {
		//Strings Zahlen
		String a = "0!";
		String b = "1!";
		String c = "2!";
		String d = "3!";
		String e = "4!";
		
		//String Rechnung
		String x = "=";
		String g = "= 1 * 2 ";
		String h = "= 1 * 2 * 3 ";
		String i = "= 1 * 2 * 3 * 4 ";
		String j = "= 1 * 2 * 3 * 4 *5 ";
		
		//String Ergebnis
		String k = "1";
		String l = "2";
		String m = "6";
		String n = "24";
		String o = "120";
		
		// String Complete
		System.out.printf( "%-5s", a); System.out.printf( "%-19s ", x); System.out.printf( "|%4s \n", k); 
		System.out.printf( "%-5s", b); System.out.printf( "%-19s ", g); System.out.printf( "|%4s \n", l); 
		System.out.printf( "%-5s", c); System.out.printf( "%-19s ", h); System.out.printf( "|%4s \n", m); 
		System.out.printf( "%-5s", d); System.out.printf( "%-19s ", i); System.out.printf( "|%4s \n", n); 
		System.out.printf( "%-5s", e); System.out.printf( "%-19s ", j); System.out.printf( "|%4s \n", o); 
		
	}
	
}
