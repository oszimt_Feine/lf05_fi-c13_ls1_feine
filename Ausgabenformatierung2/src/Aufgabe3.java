
public class Aufgabe3 {

	public static void main(String[] args) {
		//Strings Fahrenheit
		String a = "-20";
		String b = "-10";
		String c = "+0";
		String d = "+20";
		String e = "+30";
		
		//String Celsius
		double f = -28.8889;
		double g = -23.3333;
		double h = -17.7777;
		double i = -6.6667;
		double j = -1.1111;
		
		//Namen
		String k = "Fahrenheit";
		String l = "Celsius";
		
		//Tabelle
		System.out.printf( "%-13s ", k);		System.out.printf( "|%10s \n ", l);
		System.out.printf( " ----------------------\n"  );
		System.out.printf( "|%-12s ", a); 		System.out.printf( "|%10.2f \n",f); 
		System.out.printf( "|%-12s ", b);   	System.out.printf( "|%10.2f \n", g);
		System.out.printf( "|%-12s ", c);		System.out.printf( "|%10.2f \n", h);
		System.out.printf( "|%-12s ", d); 		System.out.printf( "|%10.2f \n", i);
		System.out.printf( "|%-12s ", e); 		System.out.printf( "|%10.2f \n", j); 
		
	}
	
}
