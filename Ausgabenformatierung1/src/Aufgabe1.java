
public class Aufgabe1 {

	public static void main(String[] args) {
		System.out.println("Guten Tag Felix Feine \n"		
				+ "Das ist ein “Beispielsatz“. \n"
				+ "Ein Beispielsatz  ist das.");
		// Der Unterschied zwischen print() und println besteht darin, dass println() zusätzlich einen Zeilenumbruch nach der Ausgabe einfügt.
	}

}
