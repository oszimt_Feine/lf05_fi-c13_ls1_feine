import java.util.Scanner;


public class matrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner Scanner = new Scanner(System.in);
		
		System.out.println("Welche Zahl soll ersetzt werden ? (Von 2 bis 9)");
        int zahl = Scanner.nextInt();   
        
		int i=0;
		int stelle1=0;
		int stelle2=0;
		
		System.out.println("");
		
		while (i<100) {
			
			stelle1 = i/10;
			stelle2 = i%10;
			
			
			if (i%zahl==0) {
				System.out.printf("%4s", "*");
			}		
			else if (stelle1==zahl || stelle2==zahl) {
				System.out.printf("%4s", "*");
			}			
			else if(stelle1+stelle2==zahl){
				System.out.printf("%4s", "*");
			}
			else {
				System.out.printf("%4d", i);
			}
			i++;			
			if (i%10==0) { 
				System.out.println(); 
			}
		}

        
	}
}


