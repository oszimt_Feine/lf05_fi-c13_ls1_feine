import java.util.Scanner;

public class FahrkartenautomatMITmethoden
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double tickets;
       double betragproticket;
       boolean weiterekarte=true;
       
       
while(weiterekarte=true) {
    zuZahlenderBetrag=fahrkartenbestellungErfassen();
    eingezahlterGesamtbetrag=fahrkartenBezahlen(zuZahlenderBetrag);
    fahrkartenAusgeben();
    rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);

    
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
            "vor Fahrtantritt entwerten zu lassen!\n"+
            "Wir w�nschen Ihnen eine gute Fahrt.");
    System.out.println();
    System.out.println();
    System.out.println();
}

       
    }
       

       

       public static double fahrkartenbestellungErfassen() {
      Scanner tastatur = new Scanner(System.in);
       double betragproticket = 0;
       double tickets;
       double zuZahlenderBetrag;
       int zaehler=0;
       System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
       System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1) ");
       System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2) ");
       System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3) ");

       while(betragproticket<1 || betragproticket>3){
    	   System.out.println();
           System.out.println("Bitte geben Sie die gew�hlte Nummer an: ");
           betragproticket = tastatur.nextDouble(); 
           if(betragproticket>=4 || betragproticket<=0) {
        	   System.out.println();
        	   System.out.println("Error. Bitte w�hlen Sie eine der oben genannten Nummern! ");
        	   zaehler++;
           }
           if(zaehler>2) {
        	   System.out.println();
        	   System.out.println("3 falsche Eingaben das Programm wird beendet "); 
        	   System.exit(0);
           }
       }
       if(betragproticket==1) {
    	   betragproticket=2.90;   
       }
       if(betragproticket==2) {
    	   betragproticket=8.60;   
       }
       if(betragproticket==3) {
    	   betragproticket=23.50;   
       }
       System.out.print("Anzahl der Tickets: ");
       tickets = tastatur.nextDouble();
       zuZahlenderBetrag = tickets * betragproticket;
       return zuZahlenderBetrag;
       }
       
       
       public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	   // Geldeinwurf
           // -----------
    	   Scanner tastatur = new Scanner(System.in);
    	   double eingeworfeneM�nze;
           double eingezahlterGesamtbetrag = 0.0;
           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
           {
        	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
        	   eingeworfeneM�nze = tastatur.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneM�nze;
               
           }
           return eingezahlterGesamtbetrag;
       }
       
 
       
       
      public static void fahrkartenAusgeben() {
    	  // Fahrscheinausgabe
          // -----------------
          System.out.println("\nFahrschein wird ausgegeben");
          for (int i = 0; i < 8; i++)
          {
             System.out.print("=");
             try {
   			Thread.sleep(250);
   		} catch (InterruptedException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
          }
          System.out.println("\n\n");               
    	  
      }
      
       
      
       public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	   // R�ckgeldberechnung und -Ausgabe
           // -------------------------------
    	   double r�ckgabebetrag;
    	   
           r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
           if(r�ckgabebetrag > 0.0)
           {
        	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n" , r�ckgabebetrag , "0 EURO");
        	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

               while(r�ckgabebetrag >= 1.9999) // 2 EURO-M�nzen
               {
            	  System.out.println("2,00 EURO");
    	          r�ckgabebetrag -= 2.0;
               }
               while(r�ckgabebetrag >= 0.9999) // 1 EURO-M�nzen
               {
            	  System.out.println("1,00 EURO");
    	          r�ckgabebetrag -= 1.0;
               }
               while(r�ckgabebetrag >= 0.4999) // 50 CENT-M�nzen
               {
            	  System.out.println("50 CENT");
    	          r�ckgabebetrag -= 0.5;
               }
               while(r�ckgabebetrag >= 0.1999) // 20 CENT-M�nzen
               {
            	  System.out.println("20 CENT");
     	          r�ckgabebetrag -= 0.2;
               }
               while(r�ckgabebetrag >= 0.0999) // 10 CENT-M�nzen
               {
            	  System.out.println("10 CENT");
    	          r�ckgabebetrag -= 0.1;
               }
               while(r�ckgabebetrag >= 0.0499)// 5 CENT-M�nzen
               {
            	  System.out.println("5 CENT");
     	          r�ckgabebetrag -= 0.05;
               }
               
           }
       }

      

    }
